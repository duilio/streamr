#!/usr/bin/env python
"""streamr - a distributed workflow processing platform

"""
__version__ = '0.1.0'
__license__ = 'ISC'

import os
import subprocess
import logging
from collections import defaultdict

from streamr.utils import NamedTuple, hash_key


log = logging.getLogger(__name__)


class NodeManager(object):
    """Manage nodes (abstract)

    Add nodes to the manager and run them.

    """
    def add_file_stream(self, filename):
        raise NotImplemented

    def add_processor(self, command, input, output):
        raise NotImplemented

    def run(self, name=''):
        raise NotImplemented


def stream_exists(stream):
    return os.path.exists(stream.path)


class LocalNodeManager(NodeManager):
    """Handle nodes locally"""

    def __init__(self):
        self._nodes = []
        self._queue = []
        self._deps = {}
        self._consumers = defaultdict(set)

    def add_node(self, node):
        self._nodes.append(node)

        if all(stream_exists(ostream) for ostream in node.output):
            log.info('Node %s already processed', node.key)
            return

        deps = set()
        for istream in node.input.itervalues():
            if stream_exists(istream):
                continue

            self._consumers[istream].add(node)
            deps.add(istream)

        if not deps:
            self._queue.append(node)
        else:
            self._deps[node] = deps

    def add_file_stream(self, filename):
        stream = LocalFileStream(filename)
        self.add_node(stream)

        return stream

    def add_processor(self, command, input, output):
        processor = LocalProcessor(command,
                                   input=input,
                                   output=output)

        self.add_node(processor)

        return processor.output

    def run(self, name=''):
        while self._queue:
            node = self._queue.pop()
            print 'Executing node:', node.key

            output = node.run()
            for ostream in output:
                consumers = self._consumers.pop(ostream, [])
                for consumer in consumers:
                    self._deps[consumer].remove(ostream)
                    if not self._deps[consumer]:
                        self._queue.append(consumer)
                        del self._deps[consumer]

        if self._deps:
            log.warning('Cannot execute all nodes, '
                        'some dipendencies are unsatisfied.')


class KeyIdMixin(object):
    """Identifiable objects have a unique key"""

    @property
    def key(self):
        return self._key


class Node(KeyIdMixin):
    """Base Node

    """
    def __init__(self, input={}, output=()):
        self._input = input
        self._output = output

    def run(self):
        raise NotImplemented

    @property
    def input(self):
        return self._input

    @property
    def output(self):
        return self._output


class Stream(KeyIdMixin):
    """Represent a Stream in the workflow

    Streams are used for communication between nodes.
    Do not use this class directly, use one of its subclasses.

    All streams have a `path` property, corresponding to the path to access the
    data contained.

    """
    @property
    def path(self):
        return self._path


class LocalFileStream(Node, Stream):
    """Stream for local files"""

    def __init__(self, filename):
        super(LocalFileStream, self).__init__()

        self._path = filename
        stat = os.stat(filename)
        self._key = hash_key(('file', filename, stat.st_size, stat.st_mtime))

    def run(self):
        return (self,)

    @property
    def output(self):
        return (self,)


class IntermediateStream(Stream):
    """Stream used to connect one-to-many nodes"""

    def __init__(self, key, name):
        super(IntermediateStream, self).__init__()

        self._key = hash_key(('intermediatestream', key))
        self._path = '%s_%s.dat' % (name, self._key)


class Processor(Node):
    """Processor unit

    A `Processor` node is a `Node` that produces an output.

    """
    def __init__(self, command, input, output):
        command_key = command

        self._command = command
        key = ('processor',
               command_key,
               tuple(istream.key for istream in input.itervalues()),
               output)

        self._key = hash_key(key)

        output_stream_class = self.output_stream_class
        output = NamedTuple((ostream_name,
                             output_stream_class((self.key, i), ostream_name))
                            for i, ostream_name in enumerate(output))

        super(Processor, self).__init__(input, output)


class LocalProcessor(Processor):
    """Processor unit for local activities

    Take some inputs and generate some outputs.

    The output is written into IntermediateStreams.

    """
    output_stream_class = IntermediateStream

    def __init__(self, command, input, output):
        super(LocalProcessor, self).__init__(command, input, output)

    def run(self):
        context = {}
        for input_name, istream in self._input.iteritems():
            context[input_name] = istream.path
        for ostream_name, ostream in zip(self._output._fields, self._output):
            context[ostream_name] = ostream.path

        command = self._command % context
        subprocess.check_call(command, shell=True)
        return self._output


default_manager = LocalNodeManager()


def add_file_stream(filename, manager=None):
    if manager is None:
        manager = default_manager

    return manager.add_file_stream(filename)


def run(command, input={}, output=(), manager=None):
    if manager is None:
        manager = default_manager

    return manager.add_processor(command, input, output)
