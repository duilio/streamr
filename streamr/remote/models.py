from datetime import datetime

from sqlalchemy import Table, Column, ForeignKey
from sqlalchemy import String, Integer, PickleType, DateTime, Binary
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship


class Status:
    Wait = 0
    Enqueue = 1
    Running = 2
    Succeed = 3
    Failed = 4
    Cancelled = 5


class NodeType:
    Processor = 1
    Stream = 2
    JobListener = 3


Base = declarative_base()


node_listeners = Table(
    'node_listeners', Base.metadata,
    Column('node_id', Integer, ForeignKey('nodes.id', ondelete='CASCADE')),
    Column('listener_id', Integer, ForeignKey('nodes.id', ondelete='CASCADE'))
)


class Node(Base):
    __tablename__ = 'nodes'

    id = Column(Integer, primary_key=True)
    key = Column(String(40), unique=True, nullable=True)
    status = Column(Integer)
    data = Column(PickleType)
    added = Column(DateTime, default=datetime.now)
    changed = Column(DateTime, default=datetime.now)
    type = Column(Integer)

    listeners = relationship('Node',
                             secondary=node_listeners,
                             primaryjoin=id==node_listeners.c.node_id,
                             secondaryjoin=id==node_listeners.c.listener_id,
                             backref='dependencies')

    def __repr__(self):
        return 'Node(key=%s, status=%s, data=%s)' % (self.key,
                                                     self.status,
                                                     type(self.data))


class StreamData(Base):
    __tablename__ = 'streams'

    id = Column(Integer, primary_key=True)
    stream_id = Column(Integer, ForeignKey('nodes.id', ondelete='CASCADE'),
                       unique=True)
    data = Column(Binary, nullable=True)
    added = Column(DateTime, default=datetime.now)

    stream = relationship('Node', backref='streamdata')

    def __repr__(self):
        return 'StreamData(key=%s)' % self.key


job_nodes = Table(
    'job_nodes', Base.metadata,
    Column('job_id', Integer, ForeignKey('jobs.id', ondelete='CASCADE')),
    Column('node_id', Integer, ForeignKey('nodes.id', ondelete='CASCADE'))
)


class Job(Base):
    __tablename__ = 'jobs'

    id = Column(Integer, primary_key=True)
    user = Column(String(40))
    added = Column(DateTime, default=datetime.now)
    ended = Column(DateTime, nullable=True)
    name = Column(String(200), nullable=True)
    status = Column(Integer)

    nodes = relationship('Node', secondary=job_nodes, backref='jobs')

    def __repr__(self):
        return 'Job(id=%s, user=%s, name=%s, added=%s, ended=%s)' \
            % (self.id, self.user, self.added, self.ended, self.name,)
