"""Remote excecution node managers"""

import logging
from datetime import datetime

from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy.exc import IntegrityError

from streamr.conf import settings
from streamr import NodeManager
from streamr.utils import sessioncontext
from streamr.exc import UnavailableStream
from streamr.remote.nodes import RemoteProcessor, RemoteFileStream
from streamr.remote import models
from streamr.remote.models import NodeType, Status
from streamr.remote.tasks import run_remote_process, terminate_job


# Patch pickle serializer of kombu (used by celery)
# We need to use last pickle protocol
from kombu.serialization import registry, pickle
from functools import partial
pickle_encoder = list(registry._encoders['pickle'])
pickle_encoder[2] = partial(pickle.dumps, protocol=-1)
registry._encoders['pickle'] = tuple(pickle_encoder)


log = logging.getLogger(__name__)


class RemoteNodeManager(NodeManager):
    """Manager for remote node handling

    Submit the workflow processors to the task queue.

    """
    def __init__(self, settings):
        opts = {'echo': settings.DBDEBUG}
        self._db = create_engine(settings.DBPATH, **opts)

        # SQLite workaround
        # ref: http://docs.sqlalchemy.org/en/latest/dialects/sqlite.html#serializable-transaction-isolation
        # We mostly need to acquire the RESERVED lock, so we start
        # immediate transactions
        if settings.DBPATH.startswith('sqlite:'):
            log.debug('SQLite DB detected, patching sqlalchemy...')
            from sqlalchemy import event

            @event.listens_for(self._db, 'begin')
            def do_begin(conn):
                conn.execute('BEGIN IMMEDIATE')

        models.Base.metadata.create_all(self._db)
        self.Session = sessionmaker(bind=self._db)
        self._nodes = []

    def get_session(self):
        return sessioncontext(self.Session())

    def submit_processor(self, node):
        try:
            with self.get_session() as session:
                record = models.Node(key=node.key, status=Status.Wait,
                                     data=node, type=NodeType.Processor)
                session.add(record)
        except IntegrityError:
            # node already exist, skip
            # TODO: if the node excecution was cancelled rerun it
            # TODO: rerun if the output expired
            return

        with self.get_session() as session:
            record = session.merge(record)

            input_node_keys = [istream.key
                               for istream in node.input.itervalues()]
            input_nodes = session.query(models.Node)\
                .filter(models.Node.key.in_(input_node_keys))\
                .with_lockmode('update').all()

            deps = []
            for istream in input_nodes:
                log.info('Checking dependency: %s', istream.key)
                if istream.status in (Status.Succeed, Status.Failed):
                    continue

                deps.append(istream)

            for istream in deps:
                record.dependencies.append(istream)

            for ostream in node.output:
                self._add_stream(session, ostream, Status.Wait)

            if not deps:
                record.status = Status.Enqueue

        if not deps:
            run_remote_process.delay(node)

    def submit_job(self, nodes, name):
        unfinished_status = (Status.Wait,
                             Status.Enqueue,
                             Status.Running)
        node_keys = [node.key for nodetype, node in nodes
                     if nodetype == NodeType.Processor]

        with self.get_session() as session:
            job = models.Job(user=settings.USER, name=name,
                             status=Status.Wait)
            job_node = models.Node(status=Status.Wait, data=node_keys,
                                   type=NodeType.JobListener)
            job_node.jobs.append(job)
            session.add(job)
            session.add(job_node)

        with self.get_session() as session:
            job = session.merge(job)
            job_node = session.merge(job_node)

            dependencies = session.query(models.Node)\
                .filter(models.Node.key.in_(node_keys))\
                .with_lockmode('update').all()

            for dep in dependencies:
                if dep.status in unfinished_status:
                    job_node.dependencies.append(dep)
                job.nodes.append(dep)

            if not job_node.dependencies:
                terminate_job.delay(job_node.id, node_keys)

    def terminate_job(self, job_node_id, node_keys):
        with self.get_session() as session:
            node_status = session.query(models.Node.status)\
                .distinct()\
                .filter(models.Node.key.in_(node_keys)).all()

        if Status.Failed in node_status:
            status = Status.Failed
        elif Status.Cancelled in node_status:
            status = Status.Cancelled
        else:
            status = Status.Succeed

        with self.get_session() as session:
            job_node = session.query(models.Node).get(job_node_id)
            job_node.status = status
            job_node.changed = datetime.now()

            for job in job_node.jobs:
                job.status = status
                job.ended = datetime.now()

    def _add_stream(self, session, stream, status):
        record = models.Node(key=stream.key, status=status)
        session.add(record)
        return record

    def add_processor(self, command, input, output):
        log.info('Adding processor (%s, %s, %s)', command, input, output)
        processor = RemoteProcessor(command, input, output)
        self._nodes.append((NodeType.Processor, processor))

        return processor.output

    def notify_ready(self, node, status):
        with self.get_session() as session:
            record = session.query(models.Node)\
                .filter_by(key=node.key)\
                .with_lockmode('update').one()

            record.status = status
            record.changed = datetime.now()

            # TODO: can we do a bulk-delete?
            listeners = list(record.listeners)
            for listener in listeners:
                record.listeners.remove(listener)

            listener_ids = [x.id for x in listeners]

        if not listener_ids:
            return

        with self.get_session() as session:
            awaken_nodes = session.query(models.Node)\
                .filter(models.Node.id.in_(listener_ids))\
                .filter(~models.Node.dependencies.any())\
                .filter_by(status=Status.Wait)\
                .with_lockmode('update').all()

            for n in awaken_nodes:
                log.info('Awaking node %s', n.key)

                n.status = Status.Enqueue
                n.changed = datetime.now()

                if n.type == NodeType.Processor:
                    run_remote_process.delay(n.data)
                elif n.type == NodeType.JobListener:
                    terminate_job.delay(n.id, n.data)
                else:
                    assert False, 'Invalid NodeType: %s' % n.type

    def add_file_stream(self, filename):
        stream = RemoteFileStream(filename)
        try:
            with self.get_session() as session:
                record = self._add_stream(session, stream,
                                          Status.Wait)
        except IntegrityError:
            log.info('Stream already available in the DB')
            return stream

        self.store_stream(stream, filename, record)
        return stream

    def load_stream(self, stream):
        # TODO: cache the data
        log.info('Loading stream: %s', stream.key)
        with self.get_session() as session:
            record = session.query(models.Node)\
                .filter_by(key=stream.key,
                           status=Status.Succeed).one()
            streamdata = session.query(models.StreamData)\
                .filter_by(stream=record).one()

            with open(stream.path, 'wb') as fout:
                fout.write(streamdata.data)

    def load_streams(self, streams):
        node_keys = [s.key for s in streams]
        with self.get_session() as session:
            unavailable = session.query(models.Node)\
                .filter(models.Node.key.in_(node_keys))\
                .filter(models.Node.status!=Status.Succeed).count()

        if unavailable:
            raise UnavailableStream

        for stream in streams:
            self.load_stream(stream)

    def store_stream(self, stream, filename=None, record=None):
        log.info('Storing stream: %s', stream.key)

        if not filename:
            filename = stream.path

        with self.get_session() as session:
            if not record:
                record = session.query(models.Node)\
                    .filter_by(key=stream.key).one()
            else:
                record = session.merge(record)
            record_id = record.id

        with self.get_session() as session:
            with open(filename, 'rb') as fin:
                streamdata = models.StreamData(stream_id=record_id,
                                               data=fin.read())
                session.add(streamdata)

        self.notify_ready(stream, Status.Succeed)

    def run(self, name=''):
        nodes = self._nodes[:]
        del self._nodes[:]

        for node_type, node in nodes:
            if node_type == NodeType.Processor:
                self.submit_processor(node)
            else:
                assert False, 'Wrong node type %d' % node_type

        self.submit_job(nodes, name)
