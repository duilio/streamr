"""Remote excecution nodes"""

# currently Remote nodes are just the same as local nodes
import os

from streamr import LocalFileStream
from streamr import LocalProcessor
from streamr.utils import hash_key, hash_file


class RemoteFileStream(LocalFileStream):
    def __init__(self, filename):
        super(RemoteFileStream, self).__init__(filename)

        basename, ext = os.path.splitext(os.path.basename(filename))
        self._path = '%s_%s.%s' % (basename, self.key, ext)

        stat = os.stat(filename)
        with open(filename, 'rb') as fin:
            self._key = hash_key(('file', hash_file(fin), stat.st_size))


RemoteProcessor = LocalProcessor
