"""streamr commands for the command line"""

import os
import imp
import argparse
from operator import attrgetter

from streamr.conf import settings


class Command(object):
    """Base class for command line commands

    Inherit from this class and overwrite the proper methods

    """
    def __init__(self, parser):
        """Initialize the parser"""

        pass

    def run(self, args):
        raise NotImplemented


class run(Command):
    """Run a workflow"""

    @classmethod
    def init_parser(cls, parser):
        parser.add_argument('-r', '--remote', default=False,
                            action='store_true',
                            help='Enable remote sending')
        parser.add_argument('-n', '--name', default='',
                            help='Remote job name')
        parser.add_argument('workflow')

    @classmethod
    def run(cls, args):
        import streamr
        wd = os.path.dirname(args.workflow)
        module_path = os.path.basename(args.workflow)

        if args.remote:
            import streamr.remote
            remote_manager = streamr.remote.RemoteNodeManager(settings)
            streamr.default_manager = remote_manager

        os.chdir(wd)
        imp.load_source('workflow', module_path)
        streamr.default_manager.run(args.name)


class history(Command):
    """Show job history"""

    @classmethod
    def init_parser(cls, parser):
        pass

    @classmethod
    def run(cls, args):
        from streamr.utils import sessioncontext
        from streamr.remote import RemoteNodeManager
        from streamr.remote.models import Job, Status

        manager = RemoteNodeManager(settings)
        status_map = dict((getattr(Status, k), k) for k in dir(Status))
        with sessioncontext(manager.Session()) as session:
            for job in session.query(Job).all():
                status_label = status_map[job.status]
                print 'JobId: %s | User: %s | Name: %s | Status: %s'\
                    ' | Added:%s Ended:%s' \
                    % (job.id, job.user, job.name, status_label,
                       job.added, job.ended)


class status(Command):
    """Show the status of a job"""

    @classmethod
    def init_parser(cls, parser):
        parser.add_argument('job_id', type=int, help='Job ID')

    @classmethod
    def run(cls, args):
        from streamr.remote import RemoteNodeManager
        from streamr.remote.models import Job, Node, Status, NodeType

        manager = RemoteNodeManager(settings)
        status_map = dict((getattr(Status, k), k) for k in dir(Status))
        with manager.get_session() as session:
            job = session.query(Job).get(args.job_id)
            job.nodes.sort(key=attrgetter('added'), reverse=True)

            print 'User: %s | Name: %s' % (job.user, job.name)
            print 'Status: %s | Added: %s | Ended: %s' \
                % (status_map[job.status], job.added, job.ended)
            print
            print '[Processors]'
            print

            streams = set()
            for node in job.nodes:
                if node.type != NodeType.Processor:
                    continue

                processor = node.data
                streams.update(s.key for s in processor.input.itervalues())
                streams.update(s.key for s in processor.output)

                print '%s | Status: %s | Added: %s | Last updated: %s' \
                    % (node.key, status_map[node.status],
                       node.added, node.changed)

            print
            print '[Streams]'
            print

            stream_nodes = session.query(Node)\
                .filter(Node.key.in_(streams))\
                .order_by(Node.added)

            for node in stream_nodes:
                print '%s | Status: %s | Added: %s | Last updated: %s' \
                    % (node.key, status_map[node.status],
                       node.added, node.changed)


class reset(Command):
    """Reset the db"""

    @classmethod
    def init_parser(cls, parser):
        pass

    @classmethod
    def run(cls, args):
        from streamr.remote import RemoteNodeManager
        from streamr.remote.models import Base

        manager = RemoteNodeManager(settings)
        Base.metadata.drop_all(manager._db)


class shell(Command):
    """Open a shell environment with streamr settings"""

    @classmethod
    def init_parser(cls, parser):
        parser.add_argument('-r', '--remote', default=False,
                            action='store_true',
                            help='Set up a RemoteNodeManager')
        parser.add_argument('-m', '--models', default=False,
                            action='store_true',
                            help='Import models into the default scope')

    @classmethod
    def run(cls, args):
        import streamr
        if args.remote:
            from streamr.conf import settings
            from streamr.remote import RemoteNodeManager

            manager = RemoteNodeManager(settings)
            streamr.default_manager = manager

        user_ns = {
            '__name__': '__streamr_console__',
            '__doc__': None,
            'streamr': streamr,
            'manager': streamr.default_manager
            }

        if args.models:
            import streamr.remote.models as models
            user_ns.update((k, v) for (k, v) in models.__dict__.iteritems()
                           if not k.startswith('_'))

        try:
            cls.ipyshell(args, user_ns)
        except ImportError:
            cls.pyshell(args, user_ns)

    @classmethod
    def pyshell(cls, args, user_ns):
        import code
        try:
            import readline
        except ImportError:
            pass
        else:
            import rlcompleter
            readline.set_completer(
                rlcompleter.Completer(user_ns).complete)
            readline.parse_and_binds('tab:complete')

        code.interact(local=user_ns)

    @classmethod
    def ipyshell(cls, args, user_ns):
        try:
            from IPython.frontend.terminal.embed \
                import TerminalInteractiveShell
            shell = TerminalInteractiveShell(user_ns=user_ns)
            shell.mainloop()
        except ImportError:
            try:
                from IPython.Shell import IPShell
                shell = IPShell(argv=[])
                shell.mainloop()
            except ImportError:
                # cannot import ipython
                raise


def main():
    parser = argparse.ArgumentParser()
    subparsers = parser.add_subparsers(title='Commands',
                                       description='streamr builtin commands')

    for cmd in Command.__subclasses__():
        cmd_parser = subparsers.add_parser(cmd.__name__,
                                           help=cmd.__doc__)
        cmd.init_parser(cmd_parser)
        cmd_parser.set_defaults(cmd=cmd)

    args = parser.parse_args()
    args.cmd.run(args)
