import os
import shutil
import logging
from tempfile import mkdtemp


log = logging.getLogger(__name__)


class SandBox(object):
    def __init__(self, dir=None):
        self._path = mkdtemp(prefix='sandbox_', dir=dir)

    def __enter__(self):
        self._oldwd = os.getcwd()
        os.chdir(self._path)
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        os.chdir(self._oldwd)
        log.info('Removing sandbox: %s', self._path)
        #shutil.rmtree(self._path)
