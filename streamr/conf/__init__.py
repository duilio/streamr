"""Streamr configuration"""
# TODO: use a lazy settings like django

import logging


log = logging.getLogger(__name__)


class LazySettings(object):
    def __init__(self):
        self._initialized = False

    def __getattr__(self, attr):
        if self._initialized is False:
            self._setup()
            return getattr(self, attr)
        else:
            raise AttributeError

    def _setup(self):
        self._initialized = True

        def safe_merge(d, ext):
            d.update((k, v) for (k, v) in ext.iteritems()
                     if not k.startswith('_'))

        import global_settings
        safe_merge(self.__dict__, global_settings.__dict__)

        try:
            import streamrsettings
        except ImportError:
            log.warning('Cannot import any custom configuration.'
                        ' Default settings in use.')
        else:
            safe_merge(self.__dict__, streamrsettings.__dict__)


settings = LazySettings()
