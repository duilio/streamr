# some (in)sane defaults
import os

USER = os.getlogin()[:40]
DBPATH = 'sqlite:///nodes.db'
DBDEBUG = False
