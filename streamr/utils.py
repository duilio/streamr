import hashlib
from contextlib import contextmanager


class NamedTuple(object):
    """A NamedTuple variant

    Despite `collections.namedtuple`, it doesn't define a new type.


    Example::

    >>> p = NamedTuple((('foo', 1), ('bar', 2)))
    >>> p.bar
    2
    >>> p[0]
    1

    """
    def __new__(cls, *args):
        obj = super(NamedTuple, cls).__new__(cls, *args)

        # Required, otherwise unpickling the class results in
        # an infinitive recursion on __getattr__() method
        obj._keys = {}
        obj._values = None
        obj._fields = ()
        return obj

    def __init__(self, seq):
        self._keys = {}
        values = []
        fields = []
        for i, (k, v) in enumerate(seq):
            values.append(v)
            fields.append(k)
            self._keys[k] = i
        self._values = tuple(values)
        self._fields = tuple(fields)

    def __getattr__(self, name):
        if name not in self._keys:
            raise AttributeError(name)
        return self[self._keys[name]]

    def __getitem__(self, index):
        return self._values[index]

    def __iter__(self):
        return iter(self._values)

    def __getnewargs__(self):
        return tuple(zip(self._keys, self._values)),


@contextmanager
def sessioncontext(session):
    try:
        yield session
    except:
        session.rollback()
        raise
    else:
        session.commit()
    finally:
        session.close()


def hash_key(obj):
    return hashlib.sha1(str(obj)).hexdigest()


def hash_file(fin):
    bufsize = 1024 * 1024
    k = hashlib.sha1()
    while True:
        buf = fin.read(bufsize)
        if not buf:
            break
        k.update(buf)
    return k.hexdigest()
