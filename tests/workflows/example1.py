"""Simple workflow without parameters

"""
from streamr import add_file_stream
from streamr import run

istream = add_file_stream('test.txt')

test_output, = run('grep -E "[0-9]{2}" %(test_input)s > %(ex1_test_output)s',
                   input=dict(test_input=istream),
                   output=('ex1_test_output',))
