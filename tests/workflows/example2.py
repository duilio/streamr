"""Simple workflow without parameters

"""
from streamr import add_file_stream
from streamr import run

istream = add_file_stream('test.txt')

two_digits, = run('grep -E "^[0-9]{2}$" %(test_input)s > %(ex2_two_digits)s',
                   input=dict(test_input=istream),
                   output=('ex2_two_digits',))

three_digits, = run('grep -E "^[0-9]{3}$" %(test_input)s >'
                    ' %(ex2_three_digits)s',
                    input=dict(test_input=istream),
                    output=('ex2_three_digits',))

results, = run('cat %(input_1)s %(input_2)s > %(ex2_results)s',
               input=dict(input_1=two_digits, input_2=three_digits),
               output=('ex2_results',))
