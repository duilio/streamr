#!/usr/bin/env python

import os
import streamr

try:
    from setuptools import setup
except ImportError:
    from distutils.core import setup


packages = [
    'streamr',
    'streamr.conf',
    'streamr.remote',
    ]

requires = []
scripts = [os.path.join('scripts', 'streamr')]

setup(
    name='streamr',
    version=streamr.__version__,
    description='A distribute workflow processing platform',
    long_description='A distribute workflow processing platform',
    author='Maurizio Sambati',
    packages=packages,
    package_data={'': ['LICENSE']},
    scripts=scripts,
    include_package_data=True,
    install_requires=requires,
    license=open('LICENSE').read(),
    classifiers=(
        'Development Status :: 2 - Pre-Alpha',
        'Intended Audience :: Developers',
        'Natural Language :: English',
        'License :: OSI Approved :: ISC License (ISCL)',
        'Programming Language :: Python',
        'Programming Language :: Python :: 2.7',
        'Operating System :: POSIX',
        ),
    )
